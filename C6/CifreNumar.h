#pragma once

void AfisareCifre(int);

int SumaCifre(int);
int ProdusCifre(int);
int CateCifrePare(int);
int MaximCifre(int);
int MinimCifre(int);

int Invers(int); // 1234 -> 4321
bool Palindrom(int); // simetric, 3773, 131, 6
int PrimaCifra(int); // 6543 -> 6