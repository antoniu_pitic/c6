#include "Vectori.h"
#include "Divizori.h"
#include "CifreNumar.h"
#include <iostream>
using namespace std;

void Citire(int a[], int &n)
{
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
}

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

void AfisareInvers(int a[], int n)
{
	for (int i = n - 1; i >= 0; i--) {
		cout << a[i] << " ";
	}
	cout << endl;
}

int Suma(int a[], int n)
{
	int S = 0;
	for (int i = 0; i < n; i++) {
		S += a[i];
	}
	return S;
}

int CateNumereShmmekere(int a[], int n)
{
	int ct = 0;
	for (int i = 0; i < n; i++) {
		if (Shmeker(a[i])) {
			ct++;
		}
	}
	return ct;
}

bool Shmeker(int n)
{
	n /= 10;
	n = Invers(n);
	n /= 10;
	n = Invers(n);

	return Prim(SumaDivizori(n));
}
