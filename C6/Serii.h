#pragma once

void Cresc(int);
void Descresc(int);

// 1 12 123 1234 ... 12..n
void Seria1(int);

//1
//22
//333
//4444
//....
void Seria2(int);

void Braduletz(int);

void NisteSpatii(int);
void NisteStelutze(int);
void DeAtateaOri(int);