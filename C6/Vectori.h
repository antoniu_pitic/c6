#pragma once

void Citire(int[], int&);
void Afisare(int[], int);
void AfisareInvers(int[], int);

int Suma(int[], int);
int Produs(int[], int);
int CateImpare(int[], int);
int Maxim(int[], int);
int MinimPozitive(int[], int);
int MediaAritmeticaNegative(int[], int);

//un numar shmeker  = un numar la care
//dupa ce am eliminat prima si ultima cifra, 
//suma divizorilor e numar prim
int CateNumereShmmekere(int[], int);

bool Shmeker(int);
