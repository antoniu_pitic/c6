#include "Serii.h"
#include <iostream>
using namespace std;


void Cresc(int n)
{
	for (int i = 1; i <= n; i++) {
		cout << i;
	}
}

void Descresc(int n)
{
	for (int i = n; i >=1; i--) {
		cout << i;
	}
}

// 1 12 123 1234 ... 12..n
void Seria1(int n)
{
	for (int k = 1; k <= n; k++) {
		Cresc(k);
		cout << " ";
	}
	cout << endl;
}


//1
//22
//333
//4444
//....
void Seria2(int n)
{
	for (int k = 1; k <= n; k++) {
		DeAtateaOri(k); // k de k ori
		cout << endl;
	}
}

//...*
//..***
//.*****
//*******
//...|

void Braduletz(int h)
{
	int spatii = h - 1;
	int stelutze = 1;
	for (int i = 1; i <= h; i++) {
		NisteSpatii(spatii); 
		spatii--;
		NisteStelutze(stelutze); 
		stelutze += 2;
		cout << endl;
	}
	NisteSpatii(h - 1);
	cout << "|";
}

void NisteSpatii(int n)
{	
	for (int i = 1; i <= n; i++) {
		cout << " ";
	}
}
void NisteStelutze(int n)
{	
	for (int i = 1; i <= n; i++) {
		cout << "*";
	}
}

void DeAtateaOri(int n)
{
	for (int i = 1; i <= n; i++) {
		cout << n;
	}
	/*
	int t=n;
	while(t--){
		cout << n;
	}
	*/
}
